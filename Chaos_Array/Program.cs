﻿using System;

namespace Chaos_Array
{
    class Program
    {
        
        /*
         * this method uses a try catch, in the try part it institates an array and calls the insert method
         * for two elements. 
         * then it prints the list to the console. 
         * then it calls the retrive method. 
         * catch writes out an error message if the retrive method retrives an empty element 
         */
        static void Main(string[] args)
        {
            try
            {
                ChaosArray<int> chaosList = new ChaosArray<int>();
                chaosList.Insert(20);
                chaosList.Insert(60);
                
                chaosList.Print();
                Console.WriteLine("\n");
                chaosList.Retrive();
            }
            catch(CustomExeption ex)
            {
                Console.WriteLine(ex.Message); 
            }
        }
    }
}
